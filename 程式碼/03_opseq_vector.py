#!/usr/bin/python
#-*- coding: utf-8 -*-
import os
import csv
import xlrd

#baseline opseq
BLfile = open('C:/Users/YMLab/workspace/smali/desmali/NewData 3 opseqBaselin.csv', 'r')  
BLdata = []
BLdata.insert(0, 'Apk Name')
BLdata.insert(1, 'Class')
for row in csv.reader(BLfile):
    if row != 1:
        BLdata.extend(row)
print ('>>> Baseline:', BLdata)

#Create new csv file
print ('>>> Create CSV')
Cfile = open('NewData 3 gram_vector.csv', 'w', newline='')  #create csv file
w = csv.writer(Cfile)    
w.writerow(BLdata)  # opseq baseline
   
#Run apk Info
print('>>> Start Get 3 grams opseq......')
opfile = open('C:/Users/YMLab/workspace/smali/desmali/NewData 3 gram_opseq.csv', 'r')
opdata=[]
num = 1
for rowdata in csv.reader(opfile):
    print ('App:', num)
    num += 1
    if rowdata != '[ ]':
        opseqlist = []
        opseqlist.insert(0, rowdata[0])
        opseqlist.insert(1, rowdata[1])
        
        for i in range(len(BLdata)):
            if i > 1:
                value = BLdata[i]
                if value in rowdata:
                    opcont = rowdata.count(value)
                    #print (value)
                    opseqlist.append(opcont)
                else:
                    opseqlist.append(0)
        #print ('Next')
        w.writerow(opseqlist)
    
BLfile.close()
Cfile.close()