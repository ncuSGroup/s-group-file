#!/usr/bin/python
#-*- coding: utf-8 -*-
import os
import csv
import xlrd

#baseline opseq
BLfile = open('C:/Users/YMLab/workspace/smali/desmali/NewData 4 opseqBaselin.csv', 'r')  
BLdata = []
BLdata.insert(0, 'Apk Name')
BLdata.insert(1, 'Class')
for row in csv.reader(BLfile):
    if row != 1:
        BLdata.extend(row)

print ('>>> Baseline:', BLdata)

#Create new csv file
print ('>>> Create CSV')
Cfile = open('G__test-4 gram_vector_boolean.csv', 'w', newline='')  #create csv file
w = csv.writer(Cfile)    
w.writerow(BLdata)  # opseq baseline
   
#Run apk Info
print('>>> Start Get 5grams opseq......')
opfile = open('C:/Users/YMLab/workspace/smali/desmali/Gtest 4 gram_opseq.csv', 'r')
opdata=[]
for rowdata in csv.reader(opfile):
    #print (rowdata)
    if rowdata != '[ ]':
        opseqlist = []
        opseqlist.insert(0, rowdata[0])
        opseqlist.insert(1, '?')
        
        for i in range(len(BLdata)):
            if i > 1:
                value = BLdata[i]
                if value in rowdata:
                    opseqlist.append(1)
                else:
                    opseqlist.append(0)
        
        #print (opseqlist)
        w.writerow(opseqlist)
print ("end")
BLfile.close()
Cfile.close()